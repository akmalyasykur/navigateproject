import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icons from 'react-native-vector-icons/MaterialIcons';

import HomeScreen from './src/page/HomeScreen';
import Tab1 from './src/page/Tab1';
import Tab2 from './src/page/Tab2';
import Tab3 from './src/page/Tab3';

const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        activeColor="tomato"
        inactiveColor="#c9c9c9"
        barStyle={{ backgroundColor: '#ffffff', paddingVertical: 10}}
      >
        <Tab.Screen name="Home" component={HomeScreen} options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <Icons name="home" color={color} size={26} />
          ),
        }}/>
        <Tab.Screen name="Tab1" component={Tab1} options={{
          tabBarLabel: 'Tab1',
          tabBarIcon: ({ color }) => (
            <Icons name="assessment" color={color} size={26} />
          ),
        }}/>
        <Tab.Screen name="Tab2" component={Tab2} options={{
          tabBarLabel: 'Tab2',
          tabBarIcon: ({ color }) => (
            <Icons name="bookmark" color={color} size={26} />
          ),
        }}/>
        <Tab.Screen name="Tab3" component={Tab3} options={{
          tabBarLabel: 'Tab3',
          tabBarIcon: ({ color }) => (
            <Icons name="dashboard" color={color} size={26} />
          ),
        }}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
}