import React from 'react';
import { Button, View } from 'react-native';

function Screen2({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
                title="Go to Screen3"
                onPress={() => navigation.navigate('Screen3')}
            />
            <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
    );
}
export default Screen2;