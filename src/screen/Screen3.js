import React from 'react';
import { Button, View } from 'react-native';

function Screen3({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
                title="Go to Screen4"
                onPress={() => navigation.navigate('Screen4')}
            />
            <Button title="Go back" onPress={() => navigation.goBack()} />
        </View>
    );
}

export default Screen3;