import React from 'react';
import { View, Image, Text, ScrollView, TouchableOpacity, FlatList, Button } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons';

const dataList = [
    {
        img: require('../../img/rumah/1.jpeg'),
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        img: require('../../img/rumah/2.jpeg'),
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        img: require('../../img/rumah/3.jpeg'),
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
    {
        img: require('../../img/rumah/4.jpeg'),
        id: '58694a0f-3da1-471f-bd96-145571e2asdas',
        title: 'Fourth Item',
    },
    {
        img: require('../../img/rumah/5.jpeg'),
        id: '58694a0f-3da1-471f-bd96-asd',
        title: 'Fiveth Item',
    },
];

const numColumns = 2;

formatData = (dataList, numColumns) => {
    const numberOfFullRows = Math.floor(dataList.length / numColumns);

    let numberOfElementsLastRow = dataList.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        dataList.push({ id: `blank-${numberOfElementsLastRow}`, title: `blank-${numberOfElementsLastRow}`, empty: true });
        numberOfElementsLastRow++;
    }

    return dataList;
};

TypeButton = (props) => {
    return (
        <TouchableOpacity style={{ backgroundColor: '#e8e9eb', marginLeft: 20, marginRight: 10, borderRadius: 20, }}>
            <Text style={{ color: '#b5b5b5', fontWeight: 'bold', paddingHorizontal: 20, paddingVertical: 10, }}>
                {props.title}
            </Text>
        </TouchableOpacity>
    );
}

RenderItem = ({ title, id, img, empty, navigation }) => {
    if (empty === true) {
        return <View style={{ backgroundColor: 'transparent', margin: 10, flex: 1, height: 180, borderRadius: 6 }} />;
    }
    return (
        <View elevation={15} style={{ backgroundColor: 'white', margin: 10, flex: 1, paddingBottom: 15, borderRadius: 6 }}>
            <Image style={{ width: '100%', height: 100, resizeMode: 'cover', borderTopLeftRadius: 6, borderTopRightRadius: 6 }} source={img} />
            <Text style={{ marginHorizontal: 10, marginTop: 10, fontWeight: 'bold', color: 'tomato' }}>{title}</Text>
            <Text style={{ marginHorizontal: 10, marginTop: 5, fontSize: 10, color: '#9e9e9e' }}>{id}</Text>
            <TouchableOpacity
                onPress={() => navigation.navigate('Screen2')}
            >
                <Text style={{ marginHorizontal: 10, marginTop: 10, fontSize: 10, }}>Selengkapnya</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Screen1 = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={{ margin: 20 }}>
                <View style={{ flexDirection: 'row', height: 50, justifyContent: 'space-between' }}>
                    <View style={{ flex: 1 }}>
                        <Icons name="search" color="tomato" size={26}/>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row-reverse' }}>
                        <Icons name="work" color="tomato" size={26}/>
                    </View>
                </View>
                <View style={{ height: 150, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 23, fontWeight: 'bold' }}>Find your heaven!</Text>
                    <Text style={{ marginTop: 8, fontSize: 18, }}>Here is the best place to you</Text>
                    <Text style={{ fontSize: 18, }}>find a peace</Text>
                </View>
            </View>
            <View>
                <View style={{ flexDirection: 'row' }}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TypeButton title="House" />
                        <TypeButton title="Apartment" />
                        <TypeButton title="Villa" />
                        <TypeButton title="Homestay" />
                        <TypeButton title="Land" />
                    </ScrollView>
                </View>
            </View>
            <ScrollView style={{ marginHorizontal: 10, flex: 1, marginTop: 40 , marginBottom: 0, }} showsVerticalScrollIndicator={false}>
                <FlatList
                    data={formatData(dataList, numColumns)}
                    renderItem={({ item }) => <RenderItem title={item.title} img={item.img} id={item.id} empty={item.empty} navigation={navigation}/>}
                    keyExtractor={item => item.id}
                    numColumns={numColumns}
                />
            </ScrollView>
        </View>
    );
};
