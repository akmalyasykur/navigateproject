import { Button, View } from 'react-native';

function Drawer1 ({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
                onPress={() => navigation.navigate('Drawer2')}
                title="Go to Drawer 2"
            />
        </View>
    );
}