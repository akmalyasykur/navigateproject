import React, { PureComponent } from 'react';
import { Text, View, Image, } from 'react-native';

export default class PaginateItem extends PureComponent {
    render() {
        return(
            <View elevation={6} style={{ backgroundColor: 'white', margin: 10, flex: 1, paddingBottom: 15, borderRadius: 6 }}>
                <Image style={{ width: '100%', height: 100, resizeMode: 'cover', borderTopLeftRadius: 6, borderTopRightRadius: 6 }} source={{ uri: this.props.url }} />
                <Text style={{ marginHorizontal: 10, marginTop: 10, fontWeight: 'bold', color: 'tomato' }}>{this.props.id}. {this.props.title}</Text>
            </View>
        )
    }
}