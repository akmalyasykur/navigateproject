import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList} from 'react-native';

class Tab2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            case: [],
            province: [],

        };
    }

    componentDidMount() {
        fetch('https://coronavirus-19-api.herokuapp.com/countries/indonesia', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then((json) => {
                this.setState({
                    case: json
                });
            })

        fetch('https://api.covid19api.com/summary', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then((json) => {
                this.setState({
                    province: json.Countries
                });
            })

    }

    Case = (props) => {
        return (
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>{props.title}</Text>
                <Text>{props.value}</Text>
            </View>
        )
    }

    render() {
        // console.log(this.state.province);
        return (
            <View style={styles.Container}>
                <View style={styles.TitleView}>
                    <Text style={styles.TitleText}>Covid-19 Cases in Indonesia</Text>
                    {/* <Text style={styles.SubTitleText}>by XXXXXXX</Text> */}
                </View>
                <View style={styles.MainCase}>
                    <View style={{ height: 20, justifyContent: 'space-between', flexDirection: 'row'}}>
                        <Text style={{ fontWeight: 'bold' }}>{this.state.case.country}</Text>
                        <Text style={{ fontWeight: 'bold' }}>{this.state.case.totalTests}  Kasus</Text>
                    </View>
                    <View style={{ flex: 1, marginTop: 10, }}>
                        <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row' }}>
                            <this.Case title="total positif" value={this.state.case.cases} />
                            <View style={{ width: 20 }}></View>
                            <this.Case title="positif hari ini" value={this.state.case.todayCases} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row' }}>
                            <this.Case title="total meninggal" value={this.state.case.deaths} />
                            <View style={{ width: 20 }}></View>
                            <this.Case title="meninggal hari ini" value={this.state.case.todayDeaths} />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row' }}>
                            <this.Case title="total sembuh" value={this.state.case.recovered} />
                            <View style={{ width: 20 }}></View>
                            <this.Case title="total di rawat" value={this.state.case.active} />
                        </View>
                    </View>
                </View>
                <View style={{flex:3, marginTop: 20}}>
                    <ScrollView style={{ backgroundColor: '#f2f2f2', flex: 1, padding: 20,}}>
                        <FlatList
                            data={this.state.province}
                            renderItem={({ item }) => (
                                <View style={{ flex: 1, marginBottom: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text>{item.Country}</Text>
                                    <Text>{item.TotalConfirmed}</Text>
                                </View>
                            )}
                            keyExtractor={item => item.CountryCode}
                        />
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: "white",
        padding: 20,
        paddingBottom: 0
    },
    TitleView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    TitleText: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    SubTitleText: {
        color: '#9c9c9c',
        marginTop: 8
    },
    MainCase: {  
        backgroundColor: 'white',
        flex: 1,
    },
});

export default Tab2;
