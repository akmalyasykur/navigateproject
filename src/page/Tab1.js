import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView, } from 'react-native';

class Tab1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pulsa: [],

        };
    }

    componentDidMount() {
        fetch('https://belanjapasti.com/apps-services/app-devel/ppob/pulsa_data', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'model': 'pulsa',
                'alias': 'xl'
            })
        })
            .then(res => res.json())
            // .then((json) => console.log(json))
            .then((json) => {
                this.setState({
                    pulsa: json.data
                });
            })

    }

    render() {
        // console.log(this.state.pulsa);
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{height: 100, width: '100%',justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Pulsa XL</Text>
                </View>
                <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, overflow: "visible", padding: 10 }}>
                    <FlatList
                        data={this.state.pulsa}
                        renderItem={({ item }) => (
                            <View style={{ elevation: 7, flex: 1, padding: 20, borderRadius: 6, marginVertical: 10, marginHorizontal: 10,backgroundColor: 'white' }}>
                                <Text style={{ fontSize: 12, color: '#8f8f8f'}}>{item.ppob_code}</Text>
                                <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'tomato' }}>{item.ppob_category.cat_model} {item.ppob_category.cat_name} {item.ppob_name}</Text>
                            </View>
                            )}
                        keyExtractor={item => item.ppob_id}
                        numColumns = {2}
                    />
                </ScrollView>
            </View>
        )
    }

}

export default Tab1;

