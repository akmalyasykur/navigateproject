import React, { Component } from 'react';
import { Text, View, FlatList, Image, ActivityIndicator, StatusBar } from 'react-native';

import PaginateItem from '../component/PaginateItem';

class Tab3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photos: [],
            page: 1,
        };
    }

    componentDidMount() {
        this.getData()
    }

    getData() {
        let url = 'https://jsonplaceholder.typicode.com/photos?_limit=10&_page=' + this.state.page;
        fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then((json) => {
                this.setState({
                    photos: this.state.photos.concat(json),
                    page: this.state.page + 1,
                });
            })
        fetch(url);
    }

    handleLoadMore = () => {
        this.getData();
    }

    renderFooter = () => {
        return (
            <View style={{ margin: 20 }}>
                <ActivityIndicator size="large" ></ActivityIndicator>
            </View>
        )
    }

    render() {
        console.log(this.state.photos);
        return (
            <View style={{ flex: 1, backgroundColor: 'white', padding: 20 }}>
                <StatusBar barStyle="dark-content" backgroundColor="white"></StatusBar>
                <View style={{ height: 100, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Paginate</Text>
                </View>
                <View style={{ flex: 1, backgroundColor: '#f7f7f7'}}>
                        <FlatList
                            data={this.state.photos}
                            renderItem={({ item }) => <PaginateItem url={item.url} title={item.title} id={item.id} />}
                            keyExtractor={item => item.id.toString()}
                            onEndReachedThreshold={0.5}
                            onEndReached={this.handleLoadMore}
                            ListFooterComponent={this.renderFooter}
                            removeClippedSubviews={true}
                            maxToRenderPerBatch={10}
                            updateCellsBatchingPeriod={30}
                            initialNumToRender={10}
                            windowSize={21}
                            legacyImplementation={true}
                        />
                </View>
            </View>
        );
    }
}

export default Tab3;
