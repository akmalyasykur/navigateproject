import React from 'react';
import { Button, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import Screen1 from '../screen/Screen1';
import Screen2 from '../screen/Screen2';
import Screen3 from '../screen/Screen3';
import Screen4 from '../screen/Screen4';

const Stack = createStackNavigator();

function MyStack() {
    return (
        <Stack.Navigator initialRouteName="Home" headerMode="screen"
            screenOptions={{
                headerTintColor: 'tomato',
                headerStyle: { elevation: 0, },
            }}>
            <Stack.Screen name="Screen1" component={Screen1} options={{
                headerShown: false,
            }}/>
            <Stack.Screen name="Screen2" component={Screen2} options={{
                headerTitleAlign: 'center',
            }}/>
            <Stack.Screen name="Screen3" component={Screen3} options={{
                headerTitleAlign: 'center',
            }}/>
            <Stack.Screen name="Screen4" component={Screen4} options={{
                headerTitleAlign: 'center',
            }}/>
        </Stack.Navigator>
    );
}

HomeScreen = () => {
    return (
        <MyStack />
    );
}

export default HomeScreen;
